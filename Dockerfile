# syntax=docker/dockerfile:1.5

FROM ubuntu:22.04

RUN --mount=type=cache,target=/var/lib/apt \
    set -eux; \
    apt-get update; \
    apt-get upgrade --no-install-recommends -y; \
    apt-get install --no-install-recommends -y openssh-client python3 python3-pip;

COPY requirements.txt .
RUN --mount=type=cache,target=/root/.cache/pip \
    set -eux; \
    pip install --requirement requirements.txt;

ENV ANSIBLE_COLLECTIONS_PATH=/usr/share/ansible/collections
ENV ANSIBLE_GALAXY_CACHE_DIR=/root/.cache/galaxy
COPY requirements.yml .
RUN --mount=type=cache,target=/root/.cache/galaxy \
    set -eux; \
    ansible-galaxy install -r requirements.yml;

RUN set -eux; \
    useradd --user-group --create-home --shell /bin/false ansible; \
    mkdir -m 0700 /deploy; \
    chown ansible /deploy;
USER ansible
WORKDIR /deploy

COPY --chown=ansible . .